# Leaf Lang

[Jump to Leaf Lang Standard Library](#leaf-lang-standard-library) <br>
[Jump to Leaf Lang IDE](#leaf-lang-ide) <br>
[Jump to Using Other Editors](#other-editors) <br>
[Jump to Credits](#credits)

### What is Leaf Lang?

Leaf Lang is a [stack](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)) based language that I am currently developing for a school project.

### Stack Operations

In Leaf Lang there is an assortment of builtin operations. Each manipulates the stack in one way or another and in some situations can be used in conjunction.

Here are some operations in use:

##### Adding / Removing from the stack
> ```
> # To add to the stack, simply type a number or string literal
> "Hello\n" 5 10 "World\n"  # Stack = ["Hello,\n", 5, 10, "World\n"]
>
> # To remove from the stack (also called pop), type pop
> pop  # Stack = ["Hello,\n", 5, 10]
> ```

##### Input / Output
> ```
> # pop top value from stack and print to stdout (no newline)
> "Hello World\n" print
>
> # push string read from stdout to stack
> input
> ```

##### Math operations
> ```
> # pops top two numbers from stack and pushes the sum
> 1 1.2 +  # Result: 2.2
>
> # pops top two numbers from stack and pushes the difference
> 1 1.2 -  # Result: -0.2
>
> # pops top two numbers from stack and pushes the product
> 2 1.2 *  # Result: 2.4
>
> # pops top two numbers from stack and pushes the quotient
> 1.2 2 /  # Result: 0.6
>
> # pops top two numbers from stack, rounds down to nearest integer, and pushes the remainder after division
> 15.9 13.4 %  # Result: 2
> ```

##### Evaluation operations (push boolean value to stack)
> ```
> # Equal to
> 1 1 =  # Stack = [1, 1, True]
>
> # Greater Than
> 1 1 >  # Stack = [1, 1, True, 1, 1, False]
>
> # Less Than
> 1 1 <  # Stack = [1, 1, True, 1, 1, False, 1, 1, False]
> ```

##### Conversion operations
> ```
> # pop top value from stack and convert to number
> toNumber
>
> # pop top value from stack and convert to string
> toString
> ```

##### Combining operations to get useful results
Code:
> ```
> "enter a number: " print input toNumber
> 5 % 0 = print "\n" print
> ```

Console:
> ```
> $ python3 ./src/main.py ./fake.stk
> enter a number: 15
> True
>
> $ python3 ./src/main.py ./fake.stk
> enter a number: 12
> False
> ```

### Variables

There are three data types, number, string, boolean.
Variables are separate from the stack and contain a single variable.
When a variable is called it will push its variable onto the stack.
All variables are global, static and can hold any data type.

Here are some applications of variables:

##### Declaring variables
> ```
> # Numbers
> 2 : num1
> 10.2 : num2
>
> # Strings
> "Hello World\n" : str1
>
> # Booleans
> 1 1 = : bool1   # true
> 1 2 = : bool2   # false
> ```

##### Using variables
> ```
> 10 : num1
> 20 : num2
> num1 num2 = print
> ```

### Loops and Logic

In Leaf Lang there are only 2 basic pieces of logic, if statements and while loops. Both of these are very simple, they pop the top value on the stack, if it is true they execute.

Here are some examples:

##### If statements

##### This will execute
> ```
> 1 1 = if pop pop
> 	"1 equals 1\n" print
> fi
> ```
##### This however, will not
> ```
> 1 2 = if pop pop
> 	"1 equals 2\n" print
> fi
> ```

##### While loops
> ```
> # This will print 0 -> 9 in order
> 0 : start
> 10 : end
> start end < while pop pop
> 	start print "\n" print
> 	start 1 + : start
> 	start end <
> stop
> ```

### String manipulation

Leaf Lang has a builtin macro `splitString`. `splitString` pops the top value and splits it into chars.

Example:
> ```
> "Hello" splitString  # Stack = ["H", "e", "l", "l", "o"]
> ```

### File operations

Leaf Lang has two ways of interacting with a file `read` and `write`. `read` will read the entire file at once and append it to the stack as a string. `write` will pop the top value of the stack and store it as the contents to be written. It then pops the top value again and attempts to write to the file with that name.

Example:
> ```
> "file.txt" : fileName
> fileName read : fileContents       # Read file contents
> fileName fileContents ":)" + write # Write to file the contents + :)
> ```

### Macros

Macros are similar to functions in other programming languages. The difference? they don't have a scope and don't have return values. Because everything in Leaf Lang works off of the stack, it is up to the programmer to coordinate variables passed to / from the macro. Macros cannot be defined within other macros and cannot have the same name as an existing variable.

Here is an example of how a macro can be used:
> ```
> macro hello
> 	"Hello " print print "\n" print
> end
>
> "Steve" hello
> "Amy" hello
> ```

### Other

##### Imports

Leaf Lang allows for you to import other scripts into your file by giving a path to a file in the form of a string. This allows for programs to be split into multiple files.

Example:

> adder.lf
> ```
> macro adder
> 	: adderTmpOne
> 	: adderTmpTwo
> 	adderTmpOne adderTmpTwo +
> end
> ```

> main.lf
> ```
> import "./adder.lf"
>
> macro main
> 	5 5 adder print
> end
>
> main
> ```

##### Argv and Argc

In Leaf Lang, argv and argc are handled a little different from in C or C++. Instead argc and argv are builtin functions that will append to the stack their contents.

Example:

main.lf
> ```
> argv argc printStack
> ```

> ```
> $ leaf-lang main.lf Hello World
>
> ["Hello", "World", 0]
>
> ```

##### Comments

Comments are ignored and can be placed on their own line or at the end of a line. If a comment is placed before code, the code will be ignored.

Example:
> ```
> # Comment
> 1 1 = # Another Comment
> ```

##### Exit

The command exit will exit with 0 if the stack is empty or if the top value is not a valid number. If the top value is a number it will round down to the nearest integer and exit the program.

Example:
> ```
> 10 exit
> ```

##### Print Stack

The command printStack will print the contents of the stack. It should be noted that this is for debug and is not very useful on its own.

Example:
> ```
> 1 2 3 4 5 printStack
> ```

##### Transpiling to python3 code

By using the `-c` flag when running the interpreter, instead of running your code it will transpile it to python3 code! This is great if you want to use 3rd party tools to compile the transpiled code. This code is by no means efficient, however it will be a 1:1 copy of your leaf lang program.

---

# Leaf Lang Standard Library

### Path

The Leaf Land Standard Library files can be included into any file. As of the latest releast leaf lang will search in this order for files to import: `./`, `/usr/lib/leaf-lang/`
<br>
<br>

### stdlib.lf

`stdlib.lf` will import the entire standard library. This is good for quick projects or cutting down on import commands. If you don't want to spend time remembering where the standard library macros are found you can import `stdlib.lf`.
<br>
<br>

### bool.lf

`bool.lf` provides proper `true` `false` types as well as logic gates consisting of `not` `or` `xor` `and` `nor` `xnor` and `nand`. This has proven to be very important in making code more readable.
<br>
<br>

### extended.lf

`extended.lf` provides the macro `duplicate` which will pop the top value and push it twice to the stack.
<br>
<br>

### files.lf

`files.lf` provide the macros `append` and `duplicateFile`. Normally writting to a file means to overwrite it's contents, `append` will read the file's current contents and append to them before writing to the file. `duplicateFile` will make a copy of a file.
<br>
<br>

### math.lf

`math.lf` provides the macros `abs` `floor` `ceil` `round` and `pow`. It also provides two new math constants, pi and e. It should be known that `math.lf` must have access to `bool.lf` to be used.
<br>
<br>

### print.lf

`print.lf` provides the macros `println` and `printError`. `println` takes in a single input and prints it with a newline. `printError` takes in a string and a number. It will print `Error: <string>` then exit with the provided number as an exit code. `print.lf` is a stand alone file that does not depend on any other of the standard library files.
<br>
<br>

### string.lf

`string.lf` provides the macros `format` `reverse` and `splitLines`. `format` takes input until it finds the number `0`. `format` will then return all of the strings combine, the output being a string. It should be known if you want to add `0` or a unknown number it is best to add `toString` after it. `reverse` will reverse a given string. `splitLines` will split a string by newlines. For example `["Hello\nWorld"]` becomes `["Hello", "World"]`.

Example:
> ```
> "enter a number: " print input toNumber : n
> 0 n toString "You entered: " format # Result: "You entered: <n>"
>
> 0 "World!" "Hello, " format # Result: "Hello, World!"
> ```
<br>
<br>

---

# Leaf Lang IDE

### What is the Leaf Lang IDE

The Leaf Lang IDE is a development program that simplifies the process of using Leaf Lang. If you don't want to configure a big IDE or mess around with the command line too much this is perfect for you! Leaf Lang IDE exists in the same directory as your source code and acts as a middle man between your code and the command line.

### How to get started

While in the main directory of the repository type:
> ```
> make setup_ide
> ```

From there it will provide you with a command to get started. If you ever want to resume using the editor, make your way back to the directory it creates and type:
> ```
> python3 leaf-lang-ide.py
> ```

### How do I see the output of my code?

Leaf Lang IDE should always be launched via the command line using the command provided above. The terminal used to launch the app will be where your code's output can be found. See below for example photos.

### Features

##### Assisted file creation

The editor allows for you to create new files within the program. The editor will detect the file type of the new file and change it to `.lf` to ensure compatibility with the editor.

##### Config file settings

The editor reads from a config file in the same directory as its self. That file is named `config.json`. By default it has two ***mandatory*** settings `default_file` and `interpreter_path`. Additionally it has two ***optional*** settings `background` and `foreground`. These must be formatted correctly as the editor will not catch errors (yet). The format must be as follows: `"#<6-digit hex code>"`

Here is an example config file:
> ```json
> {
> 	"default_file":"./main.lf",
> 	"interpreter_path":"../interpreter/main.py",
> 	"background":"#474747",
> 	"foreground":"#F2AF29"
> }
> ```

Note: Personally I use [this website](https://coolors.co/generate) for colors

### Pictures of use

The Editor | The Terminal
:-:|:-:
![image info](./assets/editor.png) | ![image info](./assets/terminal.png)

---

# Other Editors

Depending on if I have enough time I may or may not add more. To install the specific files needed click the link with the name of the editor and follow the instructions found in the README.md found in the directory.
<br>
<br>

There are syntax highlighting files available for:
- [Vim](editor-highlighting/vim/README.md)

---

# Credits

I have done my best to credit all major contributing sources to this project. I have also tried to give credit where possible in the source code of my project. I may have missed some parts thus I have recapped it in this section :)

### Repository Cover Image ([cover_image.png](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/assets/cover_image.png))
[craiyon.com](https://www.craiyon.com/)
<br>
<br>

### Color Pallet Generation ([editor](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/src/editor/main.py#L228))
[coolors.co](https://coolors.co/generate)
<br>
<br>

### ASCII Text Art ([editor](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/src/editor/main.py#L36), [makefile](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/Makefile#L22))
[Figlet Program](http://www.figlet.org/)
<br>
<br>

### Dirty Lambda Hack ([editor](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/src/editor/main.py#L169))
[Stack Overflow User: Loreno Heer](https://stackoverflow.com/a/40133069/18411475)
<br>
<br>

### Iota Counter ([interpreter](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/src/interpreter/main.py#L35))
Note: Tsoding has a stack based language called `porth` which was a large inspiration. I have intentionally avoided his content about the language as best I can to avoid too heavy of an influence on my own language. <br> <br>
[Tsoding Daily (youtube)](https://www.youtube.com/c/TsodingDaily) <br>
[Tsoding Daily (gitlab)](https://gitlab.com/tsoding)
<br>
<br>

### Conceptual Help ([interpreter](https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/src/interpreter/main.py#L196))
[LLVM: Kaleidoscope](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html)
