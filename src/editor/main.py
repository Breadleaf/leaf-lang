# Written by: Bradley Hutchings
# Start Date: Aug 31 2022
# End Date: ???

from importlib.machinery import SourceFileLoader
import tkinter.scrolledtext as scrolledtext
from tkinter import simpledialog
from tkinter import filedialog
from os.path import basename
from os.path import exists
from pathlib import Path
from tkinter import font
from tkinter import ttk
from json import loads
from os import system
from os import name
import webbrowser
import tkinter

#
# Make the console pretty
#

def error(message, exit_code):
    if name == "posix":
        print(f"\033[0;31mError\033[0;0m: {message}")
    else:
        print(f"Error: {message}")
    exit(exit_code)

def setup_console():
    # Trick to clear console / CMD on windows and posix systems
    system("cls" if name == "nt" else "clear")

    # I used a program call figlet to generate this text
    # figlet: http://www.figlet.org/
    print(" _                __    _                         ___ ____  _____ ")
    print("| |    ___  __ _ / _|  | |    __ _ _ __   __ _   |_ _|  _ \| ____|")
    print("| |   / _ \/ _` | |_   | |   / _` | '_ \ / _` |   | || | | |  _|  ")
    print("| |__|  __/ (_| |  _|  | |__| (_| | | | | (_| |   | || |_| | |___ ")
    print("|_____\___|\__,_|_|    |_____\__,_|_| |_|\__, |  |___|____/|_____|")
    print("                                         |___/                    ")

#
# File Menu Functions
#

def run_file(file_path, interpreter_path):
    setup_console()

    # In python you can programmatically import files as modules. This allows
    # for me to import a python file that I don't know that path of yet.
    interpreter = SourceFileLoader("main", interpreter_path).load_module()

    # Run the source code and catch the file terminating. This allows for the
    # program to continue to run as well as obtain the exit code of the source
    # code file to then tell the user. I see a lot of bigger IDE's do this and
    # figured if I could I would.
    try:
        print("Running Program...")
        interpreter.main(file_path)
    except SystemExit as ex:
        exit_code = ex.args[0]
        if exit_code == None:
            exit_code = 0
        print(f"\n\nProgram exited with code: {exit_code}")

def new_file():
    file_name = simpledialog.askstring("Input", "New file name: ")
    if file_name != None and file_name != "" and not exists(file_name):
        f_name = Path(file_name).stem
        f_suffix = Path(file_name).suffix
        if f_suffix == "" or f_suffix != ".lf":
            file_name = f_name + ".lf"
            print(f"Created {file_name} instead of {f_name + f_suffix}")
        open(file_name, "w").close()

def get_file_name():
    file_name = filedialog.askopenfilename(initialdir="./", title="Select a file:", filetypes=[("Leaf Lang", ".lf")])
    try:
        if not exists(file_name):
            return False
    except TypeError:
        return False
    return file_name

def save_file(text):
    try:
        open(get_file_name(), "w").write(text)
    except TypeError:
        pass

#
# Help Menu Functions
#

def about_leaf_lang():
    webbrowser.open("https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/README.md")

def about_ide():
    webbrowser.open("https://gitlab.com/Breadleaf/leaf-lang/-/blob/main/README.md#leaf-lang-ide")

def report_bug():
    webbrowser.open("https://gitlab.com/Breadleaf/leaf-lang/-/issues")

# This is where the app exists. I was going to make a class for the app but
# decided that it would be over kill.
def main(file_path, interpreter_path, bg_color, fg_color):
    setup_console()

    # Setup a tkinter root window
    root = tkinter.Tk()
    root.title("Leaf Lang IDE")
    root.geometry("900x600")
    root.resizable(False, False)

    # I edited one of the default fonts to make the text size a bit bigger.
    # I should also note that I picked a mono space font for better readability.
    f = font.nametofont("TkFixedFont")
    f.configure(size=14)

    # Setup theming defaults. By default I choose to make the editor
    # 'dark theme' as that is how I like it.
    root.option_add("*Font", f)
    root.option_add("*Background", bg_color)
    root.option_add("*Foreground", fg_color)
    style = ttk.Style(root)
    style.theme_use("alt")

    # Helpful label that displays the current file being edited.
    label = tkinter.Label(root, text="Currently Editing: main.lf", width=900)
    label.pack()

    # Main section of application, where the actual program is displayed.
    textbox = scrolledtext.ScrolledText(root, undo=True, borderwidth=0, relief="flat", wrap=tkinter.WORD)
    textbox.pack(expand=True, fill="both")
    textbox.insert(tkinter.END, open(file_path).read()[:-1])

    # Dirty Lambda Trick:
    # In python lambda functions can only do one thing. To counteract this you
    # can make a lambda that uses boolean logic to call multiple functions.
    # I needed to do this because I need access to class member functions within
    # main, therefor I cannot outsource to calling a function.
    #
    # How it works:
    # (lambda: (change_label(s) and False) or (open(s).read()[:-1])))()
    #
    # This declares a lambda. Normally you would assign it to a variable and
    # call that variable with <var>(). It should be noted that you cannot assign
    # variables within lambda functions hence why I could not do that. I can get
    # around this by doing (lambda: (...))() as it will call its self. Next the
    # contents of the lambda.
    #
    # (change_label(s) and False) or (open(s).read()[:-1])
    #
    # This uses boolean logic to call functions and get a return value. Because
    # I need variable s (file name as a string) I need to call a function with
    # it and read it. (change_label(s) and False) will evaluate to False.
    # (open(s).read()[:-1]) has the result of the contents of the file. This
    # means we have False or "Generic String Here" and python will evaluate this
    # and decide the result is the string.
    #
    # The lambda its self:
    # change_file_helper will return the contents of a file if the string
    # containing the file name is not 'False' if it is it will return the
    # contents of the text box the user is currently working in.
    #
    # Credit: https://stackoverflow.com/a/40133069/18411475
    # Rework explaination above
    change_label = lambda s: label.config(text=f"Currently Editing: {basename(s)}")
    change_text = lambda s: [textbox.delete(1.0, tkinter.END), textbox.insert(tkinter.END, open(s).read()[:-1]), change_label(s)]
    change_file_helper = lambda s: change_text(s) if s != False else textbox.get(1.0, tkinter.END)
    change_file = lambda: change_file_helper(get_file_name())

    # Add menu bar for quick navigation of the app
    menu_bar = tkinter.Menu(root, borderwidth=0, relief="flat")

    # File menu relates to file operations such as creation and saving.
    # It is also where you can run your code. Your code will execute in the
    # terminal used to run the program. This is where you will see the output
    # of the file as well as its exit code.
    file_menu = tkinter.Menu(menu_bar, tearoff=0)
    file_menu.add_command(label="Don't Forget To Save Your Code!", state="disabled")
    file_menu.add_command(label="Run", command=lambda: run_file(file_path, interpreter_path))
    file_menu.add_command(label="New File", command=new_file)
    file_menu.add_command(label="Open File", command=change_file)
    file_menu.add_command(label="Save File", command=lambda: save_file(textbox.get(1.0, tkinter.END)))

    # Help menu is a good source to learn about the language and editor. It also
    # links to my gitlab issues for this project so that I can fix any future
    # problems with this project.
    help_menu = tkinter.Menu(menu_bar, tearoff=0)
    help_menu.add_command(label="About Leaf Lang", command=about_leaf_lang)
    help_menu.add_command(label="About Leaf Lang IDE", command=about_ide)
    help_menu.add_command(label="Report A Bug", command=report_bug)

    # This section configures the order the buttons will appear in the menu bar.
    # This also tells the root window this is the menu bar it will want to show.
    menu_bar.add_cascade(label=" File ", menu=file_menu)
    menu_bar.add_cascade(label=" Help ", menu=help_menu)
    root.config(menu=menu_bar)

    # Start the root window putting things into motion :)
    root.mainloop()

if __name__ == "__main__":
    # Hard coded config path is intentional. I want the editor to exist in the
    # same directory as the files you are editing. It only makes sense to keep
    # everything in one place.
    config_path = "./config.json"

    # Help the user create a config file.
    if not exists(config_path):
        print("No config file found, generating...")
        while True:
            tmp_path = input("Enter a valid path to a interpreter: ")
            if exists(tmp_path):
                break
            print("Invalid path to interpreter")
        _ = '{"default_file":"./main.lf","interpreter_path":"' + tmp_path + '"}'
        open(config_path, "w").write(_)

    # Convert the config file from json to a dictionary and gather the config
    # settings. This was the most elegant solution I could come up with.
    config = loads(open(config_path).read())
    file_path = config["default_file"]
    interpreter_path = config["interpreter_path"]

    # Get custom colors from config file
    bg_color = "#464655"
    fg_color = "#EDDFEF"
    if "background" in config.keys():
        bg_color = config["background"]
    if "foreground" in config.keys():
        fg_color = config["foreground"]

    # Make sure that the file and interpreter exist.
    if not exists(file_path):
        print("No default file found, generating...")
        open(file_path, "w").close()

    if not exists(interpreter_path):
        error("Path to interpreter is invalid, please update the config file", 1)

    main(file_path, interpreter_path, bg_color, fg_color)
