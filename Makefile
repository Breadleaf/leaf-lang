compile_flags = --follow-imports --remove-output --no-pyi-file

interpreter_source = ./src/interpreter/main.py
interpreter_output = leaf
interpreter_runtime_directory = leaf-runtime-dir/

editor_source = ./src/editor/main.py
editor_output = leaf-lang-ide
editor_runtime_directory = leaf-ide-runtime-dir/

all: compile_interpreter compile_editor

setup_ide:
	@echo -e "\nRemoving Existing Runtime Directory\n"
	if test -d "$(editor_runtime_directory)"; then rm -rf $(editor_runtime_directory); fi
	@echo -e "\nMaking IDE Runtime Directory\n"
	mkdir $(editor_runtime_directory)
	@echo -e "\nSetting up config file\n"
	echo '{"default_file":"./main.lf","interpreter_path":"leaf.py"}' > $(editor_runtime_directory)config.json
	@echo -e "\nMoving Nessicary Files Into Runtime Directory\n"
	cp $(interpreter_source) $(editor_runtime_directory)$(interpreter_output).py
	cp $(editor_source) $(editor_runtime_directory)$(editor_output).py
	cp ./std/ $(editor_runtime_directory) -r
	@echo ""
	@echo " _____ _             _    ____  _                 "
	@echo "|  ___(_)_ __   __ _| |  / ___|| |_ ___ _ __    _ "
	@echo "| |_  | | '_ \ / _  | |  \___ \| __/ _ \ '_ \  (_)"
	@echo "|  _| | | | | | (_| | |   ___) | ||  __/ |_) |  _ "
	@echo "|_|   |_|_| |_|\__,_|_|  |____/ \__\___| .__/  (_)"
	@echo "                                       |_|        "
	@echo ""
	@echo "type: cd $(editor_runtime_directory) && python3 $(editor_output).py"

setup_cli:
	@echo -e "\nRemoving Existing Runtime Directory\n"
	if test -d "$(interpreter_runtime_directory)"; then rm -rf $(interpreter_runtime_directory); fi
	@echo -e "\nMaking IDE Runtime Directory\n"
	mkdir $(interpreter_runtime_directory)
	@echo -e "\nSetting up Makefile\n"
	echo -e "target=main.lf\nall:\n\tpython3 leaf.py \$$(target)" > $(interpreter_runtime_directory)Makefile
	@echo -e "\nMoving Nessicary Files Into Runtime Directory\n"
	cp $(interpreter_source) $(interpreter_runtime_directory)$(interpreter_output).py
	cp ./std/ $(interpreter_runtime_directory) -r
	@echo -e "\nMaking main.lf (entry point)\n"
	touch $(interpreter_runtime_directory)main.lf
	@echo ""
	@echo ""
	@echo " _____ _             _    ____  _                 "
	@echo "|  ___(_)_ __   __ _| |  / ___|| |_ ___ _ __    _ "
	@echo "| |_  | | '_ \ / _  | |  \___ \| __/ _ \ '_ \  (_)"
	@echo "|  _| | | | | | (_| | |   ___) | ||  __/ |_) |  _ "
	@echo "|_|   |_|_| |_|\__,_|_|  |____/ \__\___| .__/  (_)"
	@echo "                                       |_|        "
	@echo ""
	@echo "type: cd $(interpreter_runtime_directory)"
	@echo "Note: when you want to run your code type 'make' into the terminal."

compile_interpreter:
	@echo -e "\ncompiling interpreter\n"
	nuitka3 $(compile_flags) $(interpreter_source) -o $(interpreter_output)

compile_editor:
	@echo -e "\ncompiling editor\n"
	nuitka3 $(compile_flags) $(editor_source) -o $(editor_output)

clean:
	if test -f "$(editor_output)"; then rm $(editor_output); fi
	if test -f "$(interpreter_output)"; then rm $(interpreter_output); fi
	if test -d "$(editor_runtime_directory)"; then rm -rf $(editor_runtime_directory); fi
	if test -d "$(interpreter_runtime_directory)"; then rm -rf $(interpreter_runtime_directory); fi
