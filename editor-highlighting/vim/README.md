# Vim

This will add syntax highlighting to vim automatically for you! All you need to do is type the corresponding command in your terminal.
<br>
<br>

Note: this will only work on linux systems as the directories are hard coded. In the future I will try to better support other directory layouts.

### How to install
```
make install_syntax
```

### How to uninstall
```
make uninstall_syntax
```
