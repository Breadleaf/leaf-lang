" Vim syntax file
" Language: Leaf Lang
" Maintainer: Bradley Hutchings
" Latest Revision: 2 Sep 2022

if exists("b:current_syntax")
	finish
endif

" Keywords
syn keyword leaf_keywords printStack print input toNumber toString pop exit import read write splitString argv argc

" Code Blocks
syn keyword leaf_blocks if fi while stop macro end

" Comments
syn keyword leaf_notes contained TODO NOTE
syn match leaf_comment "#.*$" contains=leaf_notes

" Number Literals
syn match leaf_num '\d\+'
syn match leaf_num '[-+]\d\+'
syn match leaf_num '[-+]\d\+\.\d*'

" String Literals
syn match leaf_escape "\\[nt"]" contained
syn region leaf_string start=/\v"/ skip=/\v\\./ end=/\v"/ contains=leaf_escape

hi def link leaf_keywords Function
hi def link leaf_blocks Repeat
hi def link leaf_notes Todo
hi def link leaf_comment Comment
hi def link leaf_num Number
hi def link leaf_escape Special
hi def link leaf_string String

let b:current_syntax = "leaf"
